package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.AlumnoService;
import domain.Alumno;


@Controller
public class AlumnoController {

	@Autowired
	AlumnoService alumnoService;

	@RequestMapping(value = "/alumno", method = RequestMethod.POST)
	String savePerson(@ModelAttribute Alumno alumno, ModelMap model) {
		System.out.println("savving: " + alumno.getId());
		alumnoService.save(alumno);
		return mostrarAlumno(alumno.getId(), model);
	}
	@RequestMapping(value = "/add-alumno", method = RequestMethod.GET)
	String agregarnuevoAlumno(@RequestParam(required = false) Long id, ModelMap model) {
		Alumno alumno = id == null ? new Alumno() : alumnoService.get(id);
		model.addAttribute("alumno", alumno);
		return "add-alumno";
	}

	@RequestMapping(value = "/alumno", method = RequestMethod.GET)
	String mostrarAlumno(@RequestParam(required = false) Long id, ModelMap model) {
		if (id != null) {
			Alumno alumno = alumnoService.get(id);
			model.addAttribute("alumno", alumno);
			return "alumno";
		} else {
			Collection<Alumno> alumnos = alumnoService.getAll();
			model.addAttribute("alumnos", alumnos);
			return "alumnos";
		}
	}

}
