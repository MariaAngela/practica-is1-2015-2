package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import java.util.List;


@Entity
@Table(name = "cursos")
public class Curso implements BaseEntity<Long> {
	
	@Id
	@SequenceGenerator(name = "curso_id_generator", sequenceName = "curso_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "curso_id_generator")
	private Long id;
	
	@Column (unique = true, nullable = false, length = 64)
	private String codigo;

	@Column (unique = true, nullable = false, length = 64)
	private String nombre;

	@Column (nullable = false)
	private int creditos;

	@OneToMany(mappedBy = "curso")
	private List<Matricula> curso;

	@ManyToOne
	private Curso prerequisitos;	

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCreditos() {
		return creditos;
	}

	public void setCreditos(Integer creditos) {
		this.creditos = creditos;
	}

	public Curso getPrerequisitos() {
		return prerequisitos;
	}

	public void setPrerequisitos(Curso prerequisitos) {
		this.prerequisitos = prerequisitos;
	}


}
