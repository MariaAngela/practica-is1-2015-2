package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.CursoRepository;
import domain.Curso;

@Repository
public class JpaCursoRepository extends JpaBaseRepository<Curso, Long> implements CursoRepository{
	
	@Override
	public Curso findByNombre(String nombre) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT n FROM Curso n WHERE n.nombre = :nombre";
		TypedQuery<Curso> query = entityManager.createQuery(jpaQuery, Curso.class);
		query.setParameter("nombre", nombre);
		return getFirstResult(query);
	}
	
	@Override
	public Curso findByCodigo(String codigo) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT c FROM Curso c WHERE c.codigo = :codigo";
		TypedQuery<Curso> query = entityManager.createQuery(jpaQuery, Curso.class);
		query.setParameter("codigo", codigo);
		return getFirstResult(query);
	}

	@Override
	public Collection<Curso> findByCursoId(Long cursoId) {
		String jpaQuery = "SELECT i FROM Curso i WHERE i.id = :cursoId";
		TypedQuery<Curso> query = entityManager.createQuery(jpaQuery, Curso.class);
		query.setParameter("cursoId", cursoId);
		return query.getResultList();
	}

}
