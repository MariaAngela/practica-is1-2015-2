package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.CursoRepository;
import repository.AlumnoRepository;
import domain.Curso;

@Service
public class CursoService {
	
	@Autowired
	CursoRepository cursoRepository;
	
	@Autowired
	AlumnoRepository alumnoRepository;

	@Transactional
	public void save(Curso curso) {
		cursoRepository.persist(curso);
	}

	/*@Transactional
	public void cursosqueLleva(Collection<Long> ownerIds, Curso curso) {
		if (!ownerIds.isEmpty()) {
			Collection<Alumno> owners = alumnoRepository.findByIds(ownerIds);
			curso.setPrerequisitos((Curso) owners);
			cursoRepository.persist(curso);
		}
	}*/

	public Collection<Curso> getCursosByCursoId(Long cursoId) {
		return cursoRepository.findByCursoId(cursoId);
	}

	/*public Collection<Curso> getCursosByCursoId2(Long cursoId) {
		Curso curso = cursoRepository.find(cursoId);
		if (curso != null) {
			return curso.getCursos();
		}
		return Collections.emptyList();
	}*/

	public Collection<Curso> getCursos() {
		return cursoRepository.findAll();
	}

}
